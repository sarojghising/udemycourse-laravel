<?php

use Illuminate\Support\Facades\Route;
/* route declare the  homecontroller  */

Route::get('/', 'HomeController@home')->name('home');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/blog-post/{id}/{welcome?}', 'HomeController@blogPost')->name('blog.post');
