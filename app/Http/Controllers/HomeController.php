<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('welcome');
    }
    public function contact()
    {
        return view('contact');
    }
    public function blogPost($id, $welcome = 1)
    {

        $pages = [
            1 => [
                'title' => 'from page 1',
            ],
            2 => [

                'title' => 'from page 2',
            ],

            3 => [
                'title' => 'from page 3'
            ]
        ];
        return view('welcome', compact('id', 'welcome'));
        # code...
    }
}
